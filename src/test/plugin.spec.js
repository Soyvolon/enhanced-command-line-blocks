import { test, expect } from "@playwright/test";
import { promises as fs } from "fs";

test.describe("README.md should be rendered", () => {
  test('page should have title of "Docsify plugin playground"', async ({
    page,
  }) => {
    await page.goto("/");
    const title = await page.title();
    expect(title).toBe("Docsify plugin playground");
  });
});

test.describe("CMD replacement should work.", () => {
  test('```cmd should generate a batch code block', async ({
    page,
  }) => {
    await page.goto("/");

    
  });
});