<!-- cmd:defaultLang:batch -->
<!-- cmd:defaults:data-user="admin" -->

# Headline

> A super awesome template to develop your own Docsify plugin from scratch!

Docsify generates your documentation website on the fly. Unlike GitBook, it does not generate static html files. Instead, it smartly loads and parses your Markdown files and displays them as a website. To start using it, all you need to do is create an index.html and deploy it on GitHub Pages.

<div id="test-cmd-replacement">

## Test CMD replacement

```cmd
cd "C:\Users\Aria"
```

</div>

<div id="test-powershell">

## Test Powershell

```powershell
cd "C:\Users\Aria"
```

</div>

<div id="test-local-overrides">

## Test Local Overrides

```batch
<!-- data-user="pie" -->
<!-- data-host="crust" -->
ssh root@localhost
```

</div>

<!-- Waiting for https://github.com/PrismJS/prism/issues/3774
<div id="test-output-ind">

## Test output indicators

```powershell
ls
(out)
(out)
(out)    Directory: C:\Users\Soyvolon\source
(out)
(out)
(out)Mode                 LastWriteTime         Length Name
(out)----                 -------------         ------ ----
(out)d-----         2/12/2024  10:13 AM                repos
(out)d-----        12/18/2023   9:02 AM                Workspaces
```

</div>
-->

<div id="test-output-cfg">

## Test output cfg

```powershell
<!-- data-prompt="PS C:\Users\Soyvolon\source>" -->
<!-- data-output=" 2-10" -->
ls


    Directory: C:\Users\Soyvolon\source


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         2/12/2024  10:13 AM                repos
d-----        12/18/2023   9:02 AM                Workspaces
```
