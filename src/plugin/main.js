import { createCommandLineBlocks } from "./plugin";

const docsify = window.$docsify || {};

const props =
  {
    createCommandLineBlocks: docsify.createCommandLineBlocks ?? {}
  } || {};

docsify.plugins = [].concat(
  docsify.plugins || [],
  createCommandLineBlocks(props.createCommandLineBlocks)
);