const createCommandLineBlocks = (props) => (hook, vm) => {
  const LogNone = 0;
  const LogDebug = 1;
  const LogTrace = 2;

  const _debug = props['debugLevel'] || LogNone;
  const attributeRegex = /\<\!--.*(data-.*)=\"(.*)\".*--\>/gm

  const getAttributes = (block, pageDefaults) => {
    let attrs = [];
    let matches = block.matchAll(attributeRegex);
    for (const match of matches) {
      let start = match.index;
      let size = match[0].length;
      let key = match[1];
      let value = match[2];

      attrs.push({
        key,
        value
      });
    }

    const attrsToAdd = pageDefaults || [];
    for (let i = 0; i < attrsToAdd.length; i++) {
      const attr = attrsToAdd[i];
      attrs.push({
        key: attr['key'],
        value: attr['value']
      })
    }

    return attrs;
  }

  const getLocalProps = (html) => {
    let localProps = {
      defaultLang: props['defaultLang'] || 'batch',
      defaults: [],
      matchLangs: props['matchLangs'] || [
        'batch', 'bash', 'shell', 'powershell'
      ]
    };

    const overrides = /\<\!--.*cmd:(\w*):([A-z-]*)(?:=\"(.*)\")?.*--\>/gm

    let overrideMatches = html.matchAll(overrides);
    for (const m of overrideMatches) {
      if (_debug >= LogTrace) {
        console.log(JSON.stringify(m));
      }

      const key = m[1];
      const val = m[2];
      const prop = m[3];
      
      if (prop) {
        localProps[key].push({
          key: val,
          value: prop
        });
      } else {
        localProps[key] = val;
      }
    }

    if (_debug >= LogDebug) {
      console.log(`Page Settings: ${JSON.stringify(localProps)}`);
    }

    return localProps;
  }

  hook.beforeEach((html, next) => {
    const localProps = getLocalProps(html);

    const codeBlockRegex = /((```)(cmd)).*?```/sg

    let mod = 0;
    let matches = html.matchAll(codeBlockRegex);
    for (const match of matches) {
      if (_debug >= LogTrace) {
        console.log(JSON.stringify(match));
      }

      const start = match.index + mod;
      const size = match[0].length;

      let label = localProps['defaultLang'];

      let block = match[0].slice(0, 3) + label + match[0].slice(match[1].length);

      mod += block.length - size;

      if (_debug >= LogTrace) {
        console.log(block);
      }

      html = html.slice(0, start) + block + html.slice(start + size);
    }

    if (_debug >= LogTrace) {
      console.log(html);
    }

    next(html);
  });

  hook.afterEach((html, next) => {
    const localProps = getLocalProps(html);

    const parser = new DOMParser();
    const doc = parser.parseFromString(html ,'text/html');

    const selector = 'pre[data-lang="' 
      + localProps['matchLangs'].join('"], pre[data-lang="') 
      + '"]';

    const blocks = doc.querySelectorAll(selector);

    for (const block of blocks) {
      const code = block.querySelector('code')
      let data = code.textContent;

      const pageAttrs = localProps['defaults'];
      const localAttrs = getAttributes(data);
      const globalAttrs = props['defaults'] || [];
      let finalAttrs = localAttrs.concat(
        pageAttrs.filter(x => !localAttrs.find(e => e.key == x.key))
      );
      finalAttrs = globalAttrs.concat(
        finalAttrs.filter(x => !globalAttrs.find(e => e.key == x.key))
      );

      if (_debug >= LogTrace) {
        console.log('global attrs: ' + JSON.stringify(globalAttrs));
        console.log('page attrs: ' + JSON.stringify(pageAttrs));
        console.log('local attrs: ' + JSON.stringify(localAttrs));
        console.log('final attrs: ' + JSON.stringify(finalAttrs));
      }

      data = data.replace(attributeRegex, '').trimStart();

      code.textContent = data;

      for (const attr of finalAttrs) {
        block.setAttribute(attr.key, attr.value)
      }

      block.classList.add('command-line');
    }

    const raw = doc.documentElement.outerHTML;
    html = raw;

    if (_debug >= LogTrace) {
      console.log(html);
    }

    next(html);
  })
}

export { createCommandLineBlocks };