# enhanced-command-line-blocks

Adds some markdown support for rendering Command Line
code blocks using Prism's Command Line plugin.

```html
<script src="https://cdn.jsdelivr.net/npm/@soyvolon/docsify-enhanced-command-line-blocks/dist/plugin.js"></script>
```

## Config

```js
window.$docsify = {
    name: "plugin-playground",
    repo: "",
    plugins: [
        // Needed for Prism Autoloader to work correctly.
        function triggerAutoload(hook, vm) {
            hook.doneEach(() => {
                var main = document.getElementById('main');
                Prism.highlightAllUnder(main);
            });
        }
    ],
    // Configure the create command line blocks part of
    // the plugin
    createCommandLineBlocks: {
        // 0 - none, 1 - debug, 2 - trace
        debugLevel: 0,
        // set global defaults for key-value pairs on the <pre> element.
        defaults = [
            {
                key: "data-output",
                value: "5-10"
            }
        ],
        // The language to replace 'cmd' with.
        // Ex: ```cmd would become ```batch
        defaultLang: 'batch',
        // languages this plugin runs for:
        matchLangs: [
            // These are the default values.
            'batch', 'bash', 'shell', 'powershell'
        ]
    }
};
```

## Page and Local Overrides

You can override any config option at a page and local level.

### Page

Add `<!-- cmd:<option name>:<option key>="option value" -->` to the
top of your document. For options that dont have key-value pairs,
just use `<!-- cmd:<option name>:<option value> -->`.

ex:

```markdown
<!-- cmd:defaultLang:batch -->
<!-- cmd:defaults:data-output="7,10" -->
```

### Local

Add Add `<!-- <option key>="option value" -->` directly under the
start of a code block: \`\`\`cmd

```
```cmd
<!-- data-output="7,10" -->
```

**Local takes precedent over page which takes precedent over global.**
